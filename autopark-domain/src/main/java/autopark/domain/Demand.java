package autopark.domain;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by  01 on 21.12.2015.
 */
@Entity
@Table(name = "ap_demand")
public class Demand extends Root {
    private CarTypeEnum type;
    private CarLoadingCapacityEnum capacity;
    private String description;
    private Date creationDate;
    private Date startWorkDate;
    private User user;
    private BigDecimal settlement;



    @Enumerated(EnumType.STRING)
    public CarTypeEnum getType() {
        return type;
    }

    public void setType(CarTypeEnum type) {
        this.type = type;
    }

    @Enumerated(EnumType.STRING)
    public CarLoadingCapacityEnum getCapacity() {
        return capacity;
    }

    public void setCapacity(CarLoadingCapacityEnum capacity) {
        this.capacity = capacity;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    @ManyToOne
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getStartWorkDate() {
        return startWorkDate;
    }

    public void setStartWorkDate(Date startWorkDate) {
        this.startWorkDate = startWorkDate;
    }

    public BigDecimal getSettlement() {
        return settlement;
    }

    public void setSettlement(BigDecimal settlement) {
        this.settlement = settlement;
    }
}
