package autopark.dto;

import autopark.domain.PriceUnitEnum;

import java.math.BigDecimal;

/**
 * Created by aro on 21.03.2016.
 */
public class PriceDTO {
    private BigDecimal price;
    private PriceUnitEnum priceUnit;

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public PriceUnitEnum getPriceUnit() {
        return priceUnit;
    }

    public void setPriceUnit(PriceUnitEnum priceUnit) {
        this.priceUnit = priceUnit;
    }
}
