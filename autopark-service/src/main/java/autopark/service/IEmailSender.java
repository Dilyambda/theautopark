package autopark.service;

import autopark.dto.PasswordResetTokenDTO;
import autopark.dto.UserDTO;

/**
 * Created by  01 on 15.02.2016.
 */
public interface IEmailSender {
    void sendEmail(String to, String subject, String content, boolean isMultipart, boolean isHtml);

    void sendPasswordResetMail(String baseURL, PasswordResetTokenDTO tokenDTO);

    void sendSuccessPasswordResetMail(String baseURL, UserDTO user);

    void sendActivationEmail(String baseUrl, UserDTO user);

    void sendCreationEmail(String baseUrl, UserDTO user);

}
