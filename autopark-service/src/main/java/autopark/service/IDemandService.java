package autopark.service;

import autopark.dto.DemandDTO;
import autopark.dto.DemandSearchDTO;

import java.util.List;

/**
 * Created by  01 on 15.02.2016.
 */
public interface IDemandService {
    List<DemandDTO> getDemands();

    List<DemandDTO> getMyDemands();

    List<DemandDTO> searchDemands(DemandSearchDTO searchDTO);

}
