package autopark.service;

import autopark.dto.CarDTO;
import autopark.dto.CarSearchDTO;

import java.util.List;

/**
 * Created by  01 on 15.02.2016.
 */
public interface ICarService {
    List<CarDTO> getCars();
    CarDTO getCar(Long id);

    List<CarDTO> searchCars(CarSearchDTO dto);
    List<CarDTO> getMyCars();

}
