package autopark.service;

import autopark.AutoparkRepositoryException;

/**
 * Created by  01 on 15.02.2016.
 */
public interface IWebService {
    String getMyIp() throws AutoparkRepositoryException;
}
