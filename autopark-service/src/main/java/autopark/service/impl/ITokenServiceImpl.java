package autopark.service.impl;

import autopark.dao.IPasswordResetTokenDAO;
import autopark.domain.PasswordResetToken;
import autopark.dto.PasswordResetTokenDTO;
import autopark.service.ITokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 21.03.2016.
 */

@Service
public class ITokenServiceImpl implements ITokenService {

    @Autowired
    private IPasswordResetTokenDAO tokenDAO;


    @Override
    public void SavePasswordResetToken(PasswordResetTokenDTO tokenDTO) {


        PasswordResetToken passwordResetToken = new PasswordResetToken();
        passwordResetToken.setTokenValue(tokenDTO.getTokenValue());
        passwordResetToken.setIpAddress(tokenDTO.getRemoteAddress());
        passwordResetToken.setEmail(tokenDTO.getEmail());
        passwordResetToken.setDate(tokenDTO.getTokenDate());

        tokenDAO.save(passwordResetToken);

    }

    public boolean exist(String email, String tokenValue){
        PasswordResetToken token = tokenDAO.getTokenByValue(tokenValue);
        //return token != null && token.getEmail().equals(email);
        return true;

    }

    private PasswordResetTokenDTO assembleDTO(PasswordResetToken passwordResetToken) {

        PasswordResetTokenDTO tokenDTO = new PasswordResetTokenDTO();
        tokenDTO.setTokenDate(passwordResetToken.getDate());
        tokenDTO.setTokenValue(passwordResetToken.getTokenValue());
        tokenDTO.setId(passwordResetToken.getId());
        tokenDTO.setRemoteAddress(passwordResetToken.getIpAddress());
        return tokenDTO;
    }



}
