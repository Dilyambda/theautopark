package autopark.service;

import autopark.domain.ImageFile;
import autopark.dto.ImageFileDTO;

/**
 * 22.03.2016.
 */
public interface ImageService {
    ImageFile uploadFile(ImageFileDTO dto) throws AutoparkServiceException;
    boolean uploadMyPhoto(ImageFileDTO dto) throws AutoparkServiceException;

}
