package autopark.service;

import autopark.dto.PasswordResetTokenDTO;

/**
 * 21.03.2016.
 */
public interface ITokenService {

    void SavePasswordResetToken(PasswordResetTokenDTO tokenDTO);

    boolean exist(String email, String tokenValue);

}
