<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="utf-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="springForm"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tags" %>

<!DOCTYPE html>
<html lang="en">
    <head>
        <%@ include file="tiles/head.jsp" %>
        <link rel="stylesheet" type="text/css" href="/styles/signup.css" />
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <script src="/js/date.js"></script>
    </head>
    <body>
    <tags:headerV2 activeProfile="active"/>
        <div id="wrap">

        <div class="container">
            <springForm:form action="/signup" method="post" commandName="command" name="loginForm" cssClass="form-signup" enctype="multipart/form-data">
                <h2 class="form-signin-heading">Регистрация аккаунта</h2>
                <br/>
                <c:choose>
                    <c:when test="${res==-2}">
                        <span class="red">Ошибка. Аккаунт с таким email уже существует.</span>
                        <br/>
                    </c:when>
                    <c:when test="${res==0}">
                        <span class="green">Ваш аккаунт успешно создан.</span>
                        <br/>
                    </c:when>
                </c:choose>

                <input name="name" type="text" placeholder="ФИО" class="form-control" value="${command.name}" >
                <div><springForm:errors path="name" cssClass="error red" /></div>

                <input name="email" type="text" placeholder="Ваш Email" class="form-control" value="${command.email}" >
                <div><springForm:errors path="email" cssClass="error red" /></div>

                <input name="phone" type="text" placeholder="Ваш телефон" class="form-control" value="${command.phone}">
                <div><springForm:errors path="phone" cssClass="error red" /></div>

                <input type="text" style="display:none" />
                <input name="birthday" type="text" placeholder="Дата рождения" class="datepicker form-control" autocomplete="off"
                       value='<fmt:formatDate value="${command.birthday}" pattern="dd.MM.yyyy" />'>
                <div><springForm:errors path="birthday" cssClass="error red" /></div>
                <input type="text" style="display:none" />

                <input name="password" type="password" placeholder="Пароль" class="form-control">
                <div><springForm:errors path="password" cssClass="error red" /></div>

                <input name="passwordRetry" type="password" placeholder="Повтор пароля" class="form-control">
                <div><springForm:errors path="passwordRetry" cssClass="error red" /></div>

                <input type="file" name="myPhoto" placeholder="Моё фото" class="form-control" />

                <button class="btn btn-lg btn-primary btn-block" type="submit">ОК</button>



            </springForm:form>


            </div>
        </div>






        </div>


        <hr>
        <%@ include file="tiles/footer.jsp" %>


    </body>
</html>