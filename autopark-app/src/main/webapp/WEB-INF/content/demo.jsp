<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="utf-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="springForm" %>

<!DOCTYPE html>
<html lang="en">
<head></head>
<body>
<form action="/demo" method="post" name="demoForm" class="form-signin">

    <div class="g-recaptcha" data-sitekey="
                          6LeYRBwTAAAAACzLkHLLfgZlXETfwDbC3w9xWq0A"></div>

    <button class="btn btn-lg btn-primary btn-block" type="submit">Submit</button>
</form>


<script src="https://www.google.com/recaptcha/api.js"></script>

</body>