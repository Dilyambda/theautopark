package autopark.dao.impl;

import autopark.dao.ICarDAO;
import autopark.domain.*;
import autopark.dto.CarDTO;
import autopark.dto.CarSearchDTO;
import autopark.dto.PriceDTO;
import autopark.dto.UserDTO;
import autopark.rawconnection.Connections;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;


@Repository
public class CarDAOImpl extends RootDAOImpl<Car> implements ICarDAO {


    public CarDAOImpl() {
        super("autopark.domain.Car", Car.class);
    }

    public List<Car> getCarsHibernate() {
        return findAllList();
    }

    private static final String searchCarsQuery =
            " select " +
                    "  C.id as carId," +
                    "  C.vendor as carVendor," +
                    "  C.model as carModel," +
                    "  C.type as carType," +
                    "  C.capacity as carCapacity," +
                    "  C.year as carYear," +
                    "  C.description as carDescription," +
                    "  C.creationDate as carCreationDate," +
                    "  U.id as userId," +
                    "  U.name as userName," +
                    "  U.email as userEmail," +
                    "  U.phone as userPhone, " +
                    "  P.price as price, " +
                    "  P.priceUnit as priceUnit " +
                    "  from ap_car C " +
                    "  left join ap_user U on C.user_id=U.id " +
                    "  left join ap_price P on P.car_id=C.id " +
                    "  left join (select max(date) as mxd,car_id from ap_price group by car_id) MD on MD.car_id=C.id " +
                    "  where (p.date is null or MD.mxd=p.date ) "
            ;

    public List<CarDTO> getCarsJdbc() {
        List<CarDTO> result = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        Connection connection = Connections.getJNDIConnection();
        try {
            preparedStatement = connection.prepareStatement(searchCarsQuery);

            rs = preparedStatement.executeQuery();
            Map<Long,UserDTO> userMap = null;

            if (rs != null) {
                userMap = new HashMap<Long, UserDTO>();
                result = new ArrayList<CarDTO>();
                while (rs.next()) {
                    processCar(userMap,rs,result);
                }
            }
            return result;
        } catch (SQLException e) {
            logger.error("Failed to execute a query", e);
        } finally {
            closeAfterRaw(connection, preparedStatement, rs);
        }
        return null;
    }


    private void processCar(Map<Long, UserDTO> userMap, ResultSet rs, List<CarDTO> result) throws SQLException{
        Long userId = rs.getLong("userId");
        UserDTO userDTO = userMap.get(userId);
        if(userDTO == null){
            userDTO = new UserDTO();
            userDTO.setId(userId);
            userDTO.setName(rs.getString("userName"));
            userDTO.setEmail(rs.getString("userEmail"));
            userDTO.setPhone(rs.getString("userPhone"));
            userMap.put(userId,userDTO);
        }
        Long carId = rs.getLong("carId");
        if(carId == null || carId == 0) return;
        CarDTO carDTO = new CarDTO();
        carDTO.setId(carId);
        carDTO.setType((CarTypeEnum)extractEnum(rs,"carType", CarTypeEnum.class));
        carDTO.setVendor((CarVendorEnum)extractEnum(rs,"carVendor", CarVendorEnum.class));
        carDTO.setCapacity((CarLoadingCapacityEnum)extractEnum(rs,"carCapacity", CarLoadingCapacityEnum.class));
        carDTO.setModel(rs.getString("carModel"));
        carDTO.setDescription(rs.getString("carDescription"));
        carDTO.setYear(rs.getInt("carYear"));
        carDTO.setCreationDate(rs.getTimestamp("carCreationDate"));
        carDTO.setUser(userDTO);

        BigDecimal price = rs.getBigDecimal("price");
        if(price != null){
            PriceDTO priceDTO = new PriceDTO();
            priceDTO.setPrice(price);
            priceDTO.setPriceUnit((PriceUnitEnum)extractEnum(rs,"priceUnit", PriceUnitEnum.class));
            carDTO.setPriceDTO(priceDTO);
        }

        result.add(carDTO);

    }



    public List<CarDTO> searchCars(CarSearchDTO dto) {
        List<CarDTO> result = null;

        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        Connection connection = Connections.getJNDIConnection();
        try {

            StringBuilder mainSql = new StringBuilder(searchCarsQuery);

            List values = new ArrayList();
            if(dto != null){
                StringBuilder wherePart = new StringBuilder();

                //value not 0
                if(StringUtils.isNotEmpty(dto.getCarVendor())){
                    values.add(dto.getCarVendor());
                    wherePart.append(" C.vendor=? ");
                }

                //value not 0
                if(StringUtils.isNotEmpty(dto.getCarType())){
                    if(wherePart.length()>0) wherePart.append(" and ");
                    values.add(dto.getCarType());
                    wherePart.append(" C.type=? ");
                }

                //value not 0
                if(StringUtils.isNotEmpty(dto.getCarCapacity())){
                    if(wherePart.length()>0) wherePart.append(" and ");
                    values.add(dto.getCarCapacity());
                    wherePart.append(" C.capacity=? ");
                }

                String search = dto.getSearchText();
                if(StringUtils.isNotEmpty(search)){
                    wherePart.append(" and ");
                    wherePart.append(" ( ");
                    wherePart.append(" C.model like '%").append(search).append("%' ");
                    wherePart.append(" or C.description like '%").append(search).append("%' ");
                    wherePart.append(" or U.name like '%").append(search).append("%' ");
                    wherePart.append(" or U.email like '%").append(search).append("%' ");
                    wherePart.append(" or U.phone like '%").append(search).append("%' ");

                    wherePart.append(" ) ");
                }

                if(wherePart.length()>0){
                    mainSql.append(" and ").append(wherePart);
                }

            }

            preparedStatement = connection.prepareStatement(mainSql.toString());
            if(!values.isEmpty()){
                int i=1;
                for(Object val : values){
                    preparedStatement.setObject(i++,val);
                }
            }

            rs = preparedStatement.executeQuery();
            Map<Long,UserDTO> userMap = null;

            if (rs != null) {
                userMap = new HashMap<Long, UserDTO>();
                result = new ArrayList<CarDTO>();
                while (rs.next()) {
                    processCar(userMap,rs,result);
                }
            }
            return result;
        } catch (SQLException e) {
            logger.error("Failed to execute a query", e);
        } finally {
            closeAfterRaw(connection, preparedStatement, rs);
        }
        return null;
    }

    public List<CarDTO> getUserCars(long userId) {
        List<CarDTO> result = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        Connection connection = Connections.getJNDIConnection();
        try {
            preparedStatement = connection.prepareStatement(
                    "select " +
                            "  C.id as carId," +
                            "  C.vendor as carVendor," +
                            "  C.model as carModel," +
                            "  C.type as carType," +
                            "  C.capacity as carCapacity," +
                            "  C.year as carYear," +
                            "  C.description as carDescription," +
                            "  C.creationDate as carCreationDate," +
                            "  P.price as price, " +
                            "  P.priceUnit as priceUnit " +
                            "  from ap_car C " +
                            "  left join ap_price P on P.car_id=C.id " +
                            "  left join (select max(date) as mxd,car_id from ap_price group by car_id) MD on MD.car_id=C.id " +
                            "  where (p.date is null or MD.mxd=p.date ) and C.user_id = ?");
            preparedStatement.setLong(1,userId);
            rs = preparedStatement.executeQuery();

            if (rs != null) {
                result = new ArrayList<CarDTO>();
                while (rs.next()) {
                    CarDTO carDTO = new CarDTO();
                    carDTO.setId(rs.getLong("carId"));
                    carDTO.setType((CarTypeEnum)extractEnum(rs,"carType", CarTypeEnum.class));
                    carDTO.setVendor((CarVendorEnum)extractEnum(rs,"carVendor", CarVendorEnum.class));
                    carDTO.setCapacity((CarLoadingCapacityEnum)extractEnum(rs,"carCapacity", CarLoadingCapacityEnum.class));
                    carDTO.setModel(rs.getString("carModel"));
                    carDTO.setDescription(rs.getString("carDescription"));
                    carDTO.setYear(rs.getInt("carYear"));
                    carDTO.setCreationDate(rs.getTimestamp("carCreationDate"));

                    BigDecimal price = rs.getBigDecimal("price");
                    if(price != null){
                        PriceDTO priceDTO = new PriceDTO();
                        priceDTO.setPrice(price);
                        priceDTO.setPriceUnit((PriceUnitEnum)extractEnum(rs,"priceUnit", PriceUnitEnum.class));
                        carDTO.setPriceDTO(priceDTO);
                    }

                    result.add(carDTO);
                }
            }
            return result;
        } catch (SQLException e) {
            logger.error("Failed to execute a query", e);
        } finally {
            closeAfterRaw(connection, preparedStatement, rs);
        }
        return null;
    }


}