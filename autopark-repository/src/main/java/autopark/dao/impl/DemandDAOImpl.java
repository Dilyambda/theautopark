package autopark.dao.impl;

import autopark.dao.IDemandDAO;
import autopark.domain.CarLoadingCapacityEnum;
import autopark.domain.CarTypeEnum;
import autopark.domain.Demand;
import autopark.domain.PriceUnitEnum;
import autopark.dto.DemandDTO;
import autopark.dto.DemandSearchDTO;
import autopark.dto.PriceDTO;
import autopark.dto.UserDTO;
import autopark.rawconnection.Connections;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Repository
public class DemandDAOImpl extends RootDAOImpl<Demand> implements IDemandDAO {


    public DemandDAOImpl() {
        super("autopark.domain.Demand", Demand.class);
    }

    private static final String searchQuery =
            " select " +
                    "  C.id as carId," +
                    "  C.vendor as carVendor," +
                    "  C.model as carModel," +
                    "  C.type as carType," +
                    "  C.capacity as carCapacity," +
                    "  C.year as carYear," +
                    "  C.description as carDescription," +
                    "  C.creationDate as carCreationDate," +
                    "  U.id as userId," +
                    "  U.name as userName," +
                    "  U.email as userEmail," +
                    "  U.phone as userPhone, " +
                    "  P.price as price, " +
                    "  P.priceUnit as priceUnit " +
                    "  from ap_car C " +
                    "  left join ap_user U on C.user_id=U.id " +
                    "  left join ap_price P on P.car_id=C.id " +
                    "  left join (select max(date) as mxd,car_id from ap_price group by car_id) MD on MD.car_id=C.id " +
                    "  where (p.date is null or MD.mxd=p.date ) "
            ;



    private void processCar(Map<Long, UserDTO> userMap, ResultSet rs, List<DemandDTO> result) throws SQLException{
        Long userId = rs.getLong("userId");
        UserDTO userDTO = userMap.get(userId);
        if(userDTO == null){
            userDTO = new UserDTO();
            userDTO.setId(userId);
            userDTO.setName(rs.getString("userName"));
            userDTO.setEmail(rs.getString("userEmail"));
            userDTO.setPhone(rs.getString("userPhone"));
            userMap.put(userId,userDTO);
        }
        Long carId = rs.getLong("carId");
        if(carId == null || carId == 0) return;
        DemandDTO demandDTO = new DemandDTO();
        demandDTO.setId(carId);
        demandDTO.setType((CarTypeEnum) extractEnum(rs, "carType", CarTypeEnum.class));
        demandDTO.setCapacity((CarLoadingCapacityEnum) extractEnum(rs, "carCapacity", CarLoadingCapacityEnum.class));
        demandDTO.setDescription(rs.getString("carDescription"));
        demandDTO.setCreationDate(rs.getTimestamp("carCreationDate"));

        BigDecimal price = rs.getBigDecimal("price");
        if(price != null){
            PriceDTO priceDTO = new PriceDTO();
            priceDTO.setPrice(price);
            priceDTO.setPriceUnit((PriceUnitEnum)extractEnum(rs,"priceUnit", PriceUnitEnum.class));
            //carDTO.setPriceDTO(priceDTO);
        }

        result.add(demandDTO);

    }



    public List<DemandDTO> searchDemands(DemandSearchDTO dto) {
        List<DemandDTO> result = null;

        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        Connection connection = Connections.getJNDIConnection();
        try {

            StringBuilder mainSql = new StringBuilder(searchQuery);

            List values = new ArrayList();
            if(dto != null){
                StringBuilder wherePart = new StringBuilder();

                //value not 0
                if(dto.getCarVendor()!=null && dto.getCarVendor().length()>1){
                    values.add(dto.getCarVendor());
                    wherePart.append(" C.vendor=? ");
                }

                //value not 0
                if(dto.getCarType()!=null && dto.getCarType().length()>1){
                    if(wherePart.length()>0) wherePart.append(" and ");
                    values.add(dto.getCarType());
                    wherePart.append(" C.type=? ");
                }

                //value not 0
                if(dto.getCarCapacity()!=null && dto.getCarCapacity().length()>1){
                    if(wherePart.length()>0) wherePart.append(" and ");
                    values.add(dto.getCarCapacity());
                    wherePart.append(" C.capacity=? ");
                }

                String search = dto.getSearchText();
                if(org.apache.commons.lang.StringUtils.isNotEmpty(search)){
                    wherePart.append(" and ");
                    wherePart.append(" ( ");
                    wherePart.append(" C.model like '%").append(search).append("%' ");
                    wherePart.append(" or C.description like '%").append(search).append("%' ");
                    wherePart.append(" or U.name like '%").append(search).append("%' ");
                    wherePart.append(" or U.email like '%").append(search).append("%' ");
                    wherePart.append(" or U.phone like '%").append(search).append("%' ");

                    wherePart.append(" ) ");
                }

                if(wherePart.length()>0){
                    mainSql.append(" and ").append(wherePart);
                }

            }

            preparedStatement = connection.prepareStatement(mainSql.toString());
            if(!values.isEmpty()){
                int i=1;
                for(Object val : values){
                    preparedStatement.setObject(i++,val);
                }
            }

            rs = preparedStatement.executeQuery();
            Map<Long,UserDTO> userMap = null;

            if (rs != null) {
                userMap = new HashMap<Long, UserDTO>();
                result = new ArrayList<DemandDTO>();
                while (rs.next()) {
                    processCar(userMap,rs,result);
                }
            }
            return result;
        } catch (SQLException e) {
            logger.error("Failed to execute a query", e);
        } finally {
            closeAfterRaw(connection, preparedStatement, rs);
        }
        return null;
    }


    private static final String GET_BY_USER = "from Demand where user.id = ? order by creationDate desc ";

    @Override
    public List<Demand> getDemands(Long userId) {
        return userId==null ? null : getSession().createQuery(GET_BY_USER).setLong(0, userId).list();
    }
}