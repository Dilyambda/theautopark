package autopark.dao;


import autopark.domain.RoleUser;

public interface IRoleUserDAO extends IRootDAO<RoleUser> {
}