package autopark;

/**
 * Created by aro on 27.03.2016.
 */
public class AutoparkRepositoryException extends Exception {
    public AutoparkRepositoryException(String message, Throwable cause) {
        super(message, cause);
    }
}
