package autopark.http;

import autopark.AutoparkRepositoryException;

import javax.net.ssl.SSLContext;
import java.io.IOException;
import java.util.Map;

/**
 * Created by aro on 27.03.2016.
 */
public interface IHttpRepository {
    String call(String url);
    byte[] sendHttp(String method, String httpUrl, byte[] inputParametersString, Map<String, String> requestProperies, SSLContext sslContext) throws IOException, AutoparkRepositoryException;
    String callHttpGet(String url) throws AutoparkRepositoryException;
    String callHttpPost(String url, Map<String,String> parameters) throws AutoparkRepositoryException;

}
